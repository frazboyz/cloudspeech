var images = [
    'res/slide_1.png',
    'res/slide_2.png'
];

var index = 0;

function slide_left() {
    if ((index = (index - 1)) < 0) {
        index = (images.length - 1);
    }
    document.images.slide_image.src = images[index];
}

function slide_right() {
    if ((index = (index + 1)) >= images.length) {
        index = 0;
    }
    document.images.slide_image.src = images[index];
}